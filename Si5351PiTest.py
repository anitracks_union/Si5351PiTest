#!/usr/bin/python
#
# An attempt at making the Si5351 send Morse code at 
# a desired frequency. This does require you to have
# installed the Si5351 library from: 
# https://github.com/roseengineering/RasPi-Si5351.py.git
# at a similar directory depth.
#
# Morse code dictionary came from:
# https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/robot/morse_code/
#
# Seth McNeill
# 2015 November 16

import time
import datetime
import sys
# add the Si5351 library to the path
sys.path.insert(0, '../RasPi-Si5351.py')
import Si5351
import morse # morse.py which contains the CODE dictionary


def sendTxt(si, freq, txt, dotlen):
  si.setFreq(si.PLL_A, 0, freq)
  dashlen = 3*dotlen # seconds
  letterSpace = 3*dotlen # second
  wordSpace = 5*dotlen
  
  for letter in xmtStr:
    c = morse.CODE[letter] # find the Morse representation of a character
    for elem in c: # element (dot or dash) of character
      if(elem == '-'): # dash
        si.enableOutputs(True)
        time.sleep(dashlen)
        si.enableOutputs(False)
        time.sleep(dotlen) # pause between elements
      elif(elem == '.'): # dot
        si.enableOutputs(True)
        time.sleep(dotlen)
        si.enableOutputs(False)
        time.sleep(dotlen) # pause between elements
      else: # spaces and other stuff
        time.sleep(wordSpace)
    time.sleep(letterSpace) # pause between characters

if __name__ == "__main__":
  si = Si5351.Si5351()
  
  # reset MYCALL to your callsign
  # only uppercase letters are recognized
  xmtStr = "CQ TEST MYCALL MYCALL"
  sendData = [
    {'freq':147.4, 'txt':xmtStr, 'dit':0.25},
    {'freq':28.2294, 'txt':xmtStr, 'dit':0.25},
    {'freq':21.033, 'txt':xmtStr, 'dit':0.25},
    {'freq':14.0058, 'txt':xmtStr, 'dit':0.25},
    {'freq':10.14, 'txt':xmtStr, 'dit':0.25},
    {'freq':10.14, 'txt':xmtStr, 'dit':3},
    {'freq':7.0398, 'txt':xmtStr, 'dit':0.25},
    {'freq':7.0398, 'txt':xmtStr, 'dit':3},
    {'freq':3.5506, 'txt':xmtStr, 'dit':0.25}
    ]
  
  while(1):
    for d in sendData:
      print str(datetime.datetime.now()) + ": ",
      print "Freq: " + str(d['freq']) + ", speed: ",
      print  str(d['dit']) + ", Sending: " + d['txt']
      sendTxt(si, d['freq'], d['txt'], d['dit'])
